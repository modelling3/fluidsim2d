﻿#define NODEBUG
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <windows.h>


// OpenGL Graphics includes
#include "helper_gl.h"
#include "GL/freeglut.h"

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include "helper_functions.h"
#include "helper_cuda.h"
#include <curand.h>
#include <curand_kernel.h>
#include "src/consts.h"
#include <vector_types.h>
#include "./src/gasmodel.cuh"
#define REFRESH_DELAY     1 //ms
const unsigned int window_width = 1024;
const unsigned int window_height = 1024;


const dim3 grid(BBOXSIZE*BBOXSIZE/BLOCK_SIZE, 1, 1);
const dim3 block(BLOCK_SIZE, 1, 1);


const int particles_count = BBOXSIZE * BBOXSIZE;

GasModel* model;



// vbo variables
GLuint vbo;
GLuint cbo;
struct cudaGraphicsResource* cuda_vbo_resource; //points 
struct cudaGraphicsResource* cuda_cbo_resource; //colors
float g_fAnim = 0.0;
// mouse controls
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = 90.0, rotate_y = 0.0;
float translate_z = -3.0;
StopWatchInterface* timer = NULL;

void cleanup();
bool initGL(int* argc, char** argv);
void createBuffers(GLuint* vbo, GLuint* cbo, struct cudaGraphicsResource** vbo_res, struct cudaGraphicsResource** cbo_res, unsigned int vbo_res_flags);
void display();
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void timerEvent(int value);

// Cuda functionality
void runCuda(struct cudaGraphicsResource** vbo_resource, struct cudaGraphicsResource** cbo_resource);


bool checkHW(char* name, const char* gpuType, int dev)
{
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    strcpy(name, deviceProp.name);

    if (!STRNCASECMP(deviceProp.name, gpuType, strlen(gpuType)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

int main(int argc, char** argv)
{
    // Create the CUTIL timer
    sdkCreateTimer(&timer);

    // use command-line specified CUDA device, otherwise use device with highest Gflops/s
    int devID = findCudaDevice(argc, (const char**)argv);
    if (!initGL(&argc, argv))
    {
        return 1;
    }
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutCloseFunc(cleanup);

    createBuffers(&vbo, &cbo, &cuda_vbo_resource, &cuda_cbo_resource, cudaGraphicsMapFlagsWriteDiscard);

    glutMainLoop();

    return 0;

}

bool initGL(int* argc, char** argv)
{
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(window_width, window_height);
    glutCreateWindow("Cuda GL Interop (VBO)");
    glutDisplayFunc(display);
    glutMotionFunc(motion);
    glutTimerFunc(REFRESH_DELAY, timerEvent, 0);

    // initialize necessary OpenGL extensions
    if (!isGLVersionSupported(2, 0))
    {
        fprintf(stderr, "ERROR: Support for necessary OpenGL extensions missing.");
        fflush(stderr);
        return false;
    }

    // default initialization
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glDisable(GL_DEPTH_TEST);

    // viewport
    glViewport(0, 0, window_width, window_height);

    // projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat)window_width / (GLfloat)window_height, 0.1, 10.0);

    SDK_CHECK_ERROR_GL(); 

    return true;
}

void runCuda(struct cudaGraphicsResource** vbo_resource, struct cudaGraphicsResource** cbo_resource)
{
    // map OpenGL buffer object for writing from CUDA
    float4* dptr;
    {
        checkCudaErrors(cudaGraphicsMapResources(1, vbo_resource, 0));
        size_t num_bytes;
        checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void**)&dptr, &num_bytes, *vbo_resource));
    }

    // map OpenGL buffer object for writing from CUDA
    float4* cptr;
    {
        checkCudaErrors(cudaGraphicsMapResources(1, cbo_resource, 0));
        size_t num_bytes;
        checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void**)&cptr, &num_bytes, *cbo_resource));
    }


        PressureForceStep << <grid, block >> > (model->space, model->tempSpace, g_fAnim);
        std::swap(model->space.data, model->tempSpace.data);
        // execute the kernel
        for (int i = 0; i < 30; i++) {
            PressureStep << <grid, block >> > (model->space, model->tempSpace);
            std::swap(model->space.data, model->tempSpace.data);
        }

        PressureVelStep << <grid, block >> > (model->space, model->tempSpace);
        std::swap(model->space.data, model->tempSpace.data);


        ForceStep << <grid, block >> > (model->space, model->tempSpace, g_fAnim);
        std::swap(model->space.data, model->tempSpace.data);

        for (int i = 0; i < 20; i++) {
            ViscocityStep << <grid, block >> > (model->space, model->tempSpace);
            std::swap(model->space.data, model->tempSpace.data);
        }

        Advectionkernel << <grid, block >> > (model->space, model->tempSpace, g_fAnim);
        std::swap(model->space.data, model->tempSpace.data);


        VorticityStep1 << <grid, block >> > (model->space, model->tempSpace);
        std::swap(model->space.data, model->tempSpace.data);

        VorticityStep2 << <grid, block >> > (model->space, model->tempSpace);
        std::swap(model->space.data, model->tempSpace.data);

        cudaDeviceSynchronize();

    Renderkernel << <grid, block >> > (model->space, dptr, cptr, particles_count, g_fAnim);
    cudaDeviceSynchronize();
    // unmap buffer object
    checkCudaErrors(cudaGraphicsUnmapResources(1, vbo_resource, 0));
    checkCudaErrors(cudaGraphicsUnmapResources(1, cbo_resource, 0));
}

void createBuffers(GLuint* vbo, GLuint* cbo, struct cudaGraphicsResource** vbo_res, struct cudaGraphicsResource** cbo_res, unsigned int vbo_res_flags) {
    model = new GasModel(BBOXSIZE * BBOXSIZE);
    InitKernel << <grid, block >> > (model->space, BBOXSIZE, g_fAnim);
    cudaDeviceSynchronize();
    cudaMemcpy(model->tempSpace.data, model->space .data, sizeof(Point) * particles_count, cudaMemcpyDeviceToDevice);
    {
        assert(vbo);

        // create buffer object
        glGenBuffers(1, vbo);
        glBindBuffer(GL_ARRAY_BUFFER, *vbo);

        // initialize buffer object
        unsigned int size = particles_count * 4 * sizeof(float);
        glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // register this buffer object with CUDA
        checkCudaErrors(cudaGraphicsGLRegisterBuffer(vbo_res, *vbo, vbo_res_flags));
        SDK_CHECK_ERROR_GL();
    }
    {
        assert(cbo);

        // create buffer object
        glGenBuffers(1, cbo);
        glBindBuffer(GL_ARRAY_BUFFER, *cbo);

        // initialize buffer object
        unsigned int size = particles_count * 4 * sizeof(float);
        glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // register this buffer object with CUDA
        checkCudaErrors(cudaGraphicsGLRegisterBuffer(cbo_res, *cbo, vbo_res_flags));
        SDK_CHECK_ERROR_GL();
    }
}

void deleteBuffers()
{
    // unregister this buffer object with CUDA
    checkCudaErrors(cudaGraphicsUnregisterResource(cuda_vbo_resource));
    glBindBuffer(1, vbo);
    glDeleteBuffers(1, &vbo);
    vbo = 0;

    checkCudaErrors(cudaGraphicsUnregisterResource(cuda_cbo_resource));
    glBindBuffer(1, cbo);
    glDeleteBuffers(1, &cbo);
    cbo = 0;
}

void display()
{
    sdkStartTimer(&timer);

    // run CUDA kernel to generate vertex positions
    runCuda(&cuda_vbo_resource, &cuda_cbo_resource);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, translate_z);
    glRotatef(rotate_x, 1.0, 0.0, 0.0);
    glRotatef(rotate_y, 0.0, 1.0, 0.0);

    // render from the vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo); 
    glVertexPointer(4, GL_FLOAT, 0, 0); 

    glBindBuffer(GL_ARRAY_BUFFER, cbo);
    glColorPointer(4, GL_FLOAT, 0, 0);


    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDrawArrays(GL_POINTS, 0, particles_count);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    glutSwapBuffers();
    g_fAnim += DT/50;
    sdkStopTimer(&timer);
}

void timerEvent(int value)
{
    if (glutGetWindow())
    {
        glutPostRedisplay();
        glutTimerFunc(REFRESH_DELAY, timerEvent, 0);
    }
}

void cleanup()
{
    sdkDeleteTimer(&timer);
    deleteBuffers();
}

void mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        mouse_buttons |= 1 << button;
    }
    else if (state == GLUT_UP)
    {
        mouse_buttons = 0;
    }

    mouse_old_x = x;
    mouse_old_y = y;
}

void motion(int x, int y)
{
    float dx, dy;
    dx = (float)(x - mouse_old_x);
    dy = (float)(y - mouse_old_y);

    if (mouse_buttons & 1)
    {
        rotate_x += dy * 0.2f;
        rotate_y += dx * 0.2f;
    }
    else if (mouse_buttons & 4)
    {
        translate_z += dy * 0.01f;
    }

    mouse_old_x = x;
    mouse_old_y = y;
}
