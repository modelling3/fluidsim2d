#pragma once
#include "cuda_vectors.cuh"



struct DeviceSpaceAccessor2D {
private:
	int sizex;
	int sizey;
public:
	Point* data;
	float dxy;
	DeviceSpaceAccessor2D() {}
	DeviceSpaceAccessor2D(int sizex, int sizey, float dxy) {
		this->sizex = sizex;
		this->sizey = sizey;
		this->dxy = dxy;
		cudaMalloc(&data, sizex * sizey * sizeof(Point));
	}

	__device__ Point* Get(int id) {
		return this->data + id;
	}

	__device__ Point* Get(int idx, int idy) {
		bool borderX = false;
		bool borderY = false;
		if (idx < 0) {
			idx = 0;
			borderX = true;
		}
		if (idy < 0) {
			idy = 0;
			borderY = true;
		}
		if (idx >= this->sizex) {
			idx = this->sizex - 1;
			borderX = true;
		}
		if (idy >= this->sizey) {
			idy = this->sizey-1;
			borderY = true;
		}
		Point* val = Get(idx * this->sizex + idy);

		if (borderX || borderY) {
			val->velocity = float2{ 0, 0 };
			if (idx == 0) {
				val->pressure = 0;
			}
			else if (idx == this->sizex - 1) {
				val->pressure = 0;
			}
			else {
				val->pressure = 0;
			}
		}
		return val;
	}


	__device__ Point* Get(float2 pos) {
		int idx = (pos.x / this->dxy);
		int idy = (pos.y / this->dxy);
		return Get(idx, idy);
	}
	__device__ float2 GetPressureGradient(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;

		float dpdx = (Get(idx + 1, idy)->pressure - Get(idx - 1, idy)->pressure) / (2 * this->dxy);
		float dpdy = (Get(idx, idy + 1)->pressure - Get(idx, idy - 1)->pressure) / (2 * this->dxy);
		return make_float2(dpdx, dpdy);
	}
	__device__ Point InterpolatedVelocity(float2 pos) {

		float x0 = pos.x;
		float y0 = pos.y;

		float x1 = ((float)(int)(pos.x / this->dxy)) * this->dxy;
		float y1 = ((float)(int)(pos.y / this->dxy)) * this->dxy;

		float x2 = ((float)(int)(pos.x / this->dxy) + 1) * this->dxy;
		float y2 = ((float)(int)(pos.y / this->dxy) + 1) * this->dxy;

		float tx1 = (x2 - x0) / (x2-x1);
		float tx2 = (x0 - x1) / (x2-x1);
		float ty3 = (y2 - y0) / (y2-y1);
		float ty4 = (y0 - y1) / (y2-y1);

		Point* _q0 = Get(pos);

		Point* q1 = Get(make_float2(x1, y1));
		Point* q2 = Get(make_float2(x1, y2));
		Point* q3 = Get(make_float2(x2, y1));
		Point* q4 = Get(make_float2(x2, y2));


		Point result;
		result.velocity = (q1->velocity * tx1 + q3->velocity * tx2) * ty3 + (q2->velocity * tx1 + q4->velocity * tx2) * ty4;
		result.color = (q1->color * tx1 + q3->color * tx2) * ty3 + (q2->color * tx1 + q4->color * tx2) * ty4;

		return result;
	}

	__device__ float GetVelocityDivergence(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;


		float dudx = dot(
			(Get(idx + 1, idy)->velocity - Get(idx - 1, idy)->velocity),
			{ 1.0f,0 }) / (2 * this->dxy);
		float dudy = dot(
			(Get(idx, idy + 1)->velocity - Get(idx, idy - 1)->velocity),
			{ 0,1.0f }) / (2 * this->dxy);

		return dudx + dudy;
	}


	__device__ float2 GetVelocityAdvection(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;


		float2 vel = Get(idx, idy)->velocity;
		
		float duxdx = (Get(idx + 1, idy)->velocity.x - Get(idx - 1, idy)->velocity.x) / (2 * this->dxy);
		float duydx = (Get(idx + 1, idy)->velocity.y - Get(idx - 1, idy)->velocity.y) / (2 * this->dxy);

		float duxdy = (Get(idx, idy + 1)->velocity.x - Get(idx, idy - 1)->velocity.x) / (2 * this->dxy);
		float duydy = (Get(idx, idy + 1)->velocity.y - Get(idx, idy - 1)->velocity.y) / (2 * this->dxy);

		float2 result{
			vel.x* duxdx + vel.y * duxdy,
			vel.x* duydx + vel.y * duydy,
		};

		return result;
	}

	__device__ float3 GetVelocityRotor(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;

		float duydx = (Get(idx + 1, idy)->velocity.y - Get(idx - 1, idy)->velocity.y) * (1.0f / (2 * this->dxy));
		float duxdy = (Get(idx, idy + 1)->velocity.x - Get(idx, idy - 1)->velocity.x) * (1.0f / (2 * this->dxy));

		return { 0, 0, duydx - duxdy };
	}
	__device__ float2 GetVorticityAbsGradientZ(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;

		float duzdx = (abs(Get(idx + 1, idy)->vorticity.z) - abs(Get(idx - 1, idy)->vorticity.z)) * (1.0f / (2 * this->dxy));
		float duzdy = (abs(Get(idx, idy + 1)->vorticity.z) - abs(Get(idx, idy - 1)->vorticity.z)) * (1.0f / (2 * this->dxy));

		return { duzdx, duzdy };
	}

	__device__ float JacobiPressure(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;

		Point* q0 = Get(idx, idy);

		Point* q1 = Get(idx - 1, idy);
		Point* q2 = Get(idx + 1, idy);
		Point* q3 = Get(idx, idy - 1);
		Point* q4 = Get(idx, idy + 1);

		Point* q5 = Get(idx - 1, idy - 1);
		Point* q6 = Get(idx + 1, idy + 1);
		Point* q7 = Get(idx + 1, idy - 1);
		Point* q8 = Get(idx - 1, idy + 1);

		float veldiv = GetVelocityDivergence(id);

		float alpha = - 1.0f * (DENSITY * DENSITY);
		float beta = 8.0;

		float x0 = (
			(q1->pressure + q2->pressure + q3->pressure + q4->pressure) +
			(q5->pressure + q6->pressure + q7->pressure + q8->pressure) +
			alpha * veldiv
			) / beta;
		return x0;
	}

	__device__ float2 JacobiViscocity(int id) {
		int idx = id / BBOXSIZE;
		int idy = id % BBOXSIZE;

		Point* q0 = Get(idx, idy);

		Point* q1 = Get(idx - 1, idy);
		Point* q2 = Get(idx + 1, idy);
		Point* q3 = Get(idx, idy - 1);
		Point* q4 = Get(idx, idy + 1);

		
		Point* q5 = Get(idx - 1, idy - 1);
		Point* q6 = Get(idx + 1, idy + 1);
		Point* q7 = Get(idx + 1, idy - 1);
		Point* q8 = Get(idx - 1, idy + 1);

		float alpha = VISCOCITY * VISCOCITY / DT;
		float rbetta = 1.0f / (8.0f + alpha);

		float2 x0 = (
			(q1->velocity + q2->velocity + q3->velocity + q4->velocity) +
			(q5->velocity + q6->velocity + q7->velocity + q8->velocity) +
			q0->velocity * alpha
			) * rbetta;
		return x0;
	}


};