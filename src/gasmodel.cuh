﻿#pragma once
#include "cuda_vectors.cuh"
#include <curand.h>
#include <curand_kernel.h>
#include "consts.h"
#include "space_accessor.cuh"


struct GasModel {
	int size;
	float dxy;
public:
	DeviceSpaceAccessor2D space;
	DeviceSpaceAccessor2D tempSpace;
	GasModel(int size) {
		this->size = size;
		this->dxy = 1.0f;
		this->space = DeviceSpaceAccessor2D(BBOXSIZE, BBOXSIZE, this->dxy);

		this->tempSpace = DeviceSpaceAccessor2D(BBOXSIZE, BBOXSIZE, this->dxy);
	}
};


__global__ void InitKernel(DeviceSpaceAccessor2D space, unsigned int particles_count, float time) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);
	int idx = id / BBOXSIZE;
	int idy = id % BBOXSIZE;

	curandState_t randomState;
	curand_init((unsigned long long)(time + id), 0ULL, 0ULL, &randomState);

	Point* point = space.Get(id);
	point->position.x = (float)idx * space.dxy;
	point->position.y = (float)idy * space.dxy;

	point->velocity.x = 0;//curand_normal(&randomState);
	point->velocity.y = 0;//curand_normal(&randomState);
	point->pressure = 0;


	float angle1, angle2, angle3;
	{
		float2 dir = point->position - float2{ BBOXSIZE / 2, BBOXSIZE / 2 };
		dir = dir * (1.0f / length(dir));
		angle1 = acos(dot(dir, { 0,1 })) / 5.28;
	}

	{
		float2 dir = point->position - float2{ BBOXSIZE / 2, BBOXSIZE / 2 };
		dir = dir * (1.0f / length(dir));
		angle2 = acos(dot(dir, { 0,-1 })) / 5.28;
	}
	{
		float2 dir = point->position - float2{ BBOXSIZE / 2, BBOXSIZE / 2 };
		dir = dir * (1.0f / length(dir));
		angle3 = acos(dot(dir, { 1/sqrtf(2) ,1 / sqrtf(2)})) / 5.28;
	}
	point->color = {
		angle1,
		angle2,
		angle3
		};

}

__global__ void Renderkernel(DeviceSpaceAccessor2D space, float4* vbos, float4* cbos, unsigned int particles_count, float time) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* point = space.Get(id);
	vbos[id].x = point->position.x / BBOXSIZE/ space.dxy - 0.5;
	vbos[id].y = point->pressure / 100;
	vbos[id].z = point->position.y / BBOXSIZE / space.dxy - 0.5;
	vbos[id].w = 1;

	float2 adv = space.GetVelocityAdvection(id);
	float3 rotor = space.GetVelocityRotor(id);
	
	cbos[id].x = point->color.x;
	cbos[id].y = point->color.y;
	cbos[id].z = point->color.z;
	
	//cbos[id].x = abs(adv.x) * 100;
	//cbos[id].y = abs(adv.y) * 100;
	//cbos[id].x = 0;
	//cbos[id].y = 0;
	//cbos[id].z = abs(rotor.z) * 1;
	//cbos[id].z = 0;
	cbos[id].w = 1;
}

__global__ void ForceStep(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace, float time) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);

	float2 dir = source->position - float2{ BBOXSIZE / 2, BBOXSIZE / 2 };
	float l = length(dir);

	if (l > 4 && l < 5) {
		//target->velocity = source->velocity + float2{ dir.y, -dir.x } * (1.0f / l) * DT * sinf(time*200) * 40000;
	}
	else {
		target->velocity = source->velocity;
	}
}

__global__ void PressureForceStep(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace, float time) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);

	float2 dir = source->position - float2{ BBOXSIZE / 2, BBOXSIZE / 2 };
	float l = length(dir);
	if (l < 5) {
		target->pressure = source->pressure - 20 * DT;
	}
	else {
		target->pressure = source->pressure;
	}
}


__global__ void ViscocityStep(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);
	target->velocity = space.JacobiViscocity(id);
}


__global__ void PressureStep(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);
	target->pressure = space.JacobiPressure(id);
}
__global__ void PressureVelStep(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);

	float2 pressure_grad = space.GetPressureGradient(id);


	target->velocity = source->velocity - pressure_grad * DT;
}

__global__ void VorticityStep1(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);

	float3 vort = space.GetVelocityRotor(id);
	target->vorticity = vort;
}

__global__ void VorticityStep2(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);

	Point* source = space.Get(id);
	Point* target = resultSpace.Get(id);

	float2 vortGrad = space.GetVorticityAbsGradientZ(id);
	vortGrad = vortGrad * (1.0f / length(vortGrad));

	float3 result = vecmul({ vortGrad.x, vortGrad.y, 0 }, source->vorticity);
	
	target->velocity.x += result.x * DT * VORTICITY;
	target->velocity.y += result.y * DT * VORTICITY;

}


__global__ void Advectionkernel(DeviceSpaceAccessor2D space, DeviceSpaceAccessor2D resultSpace, float time) {
	int id = (blockIdx.x * blockDim.x + threadIdx.x);
	int idx = id / BBOXSIZE;
	int idy = id % BBOXSIZE;

	Point* point = space.Get(id);
	Point* target = resultSpace.Get(id);

	Point mix = space.InterpolatedVelocity(point->position - point->velocity * DT);


	target->velocity = mix.velocity * (1 / (1 + DECAY * DT));
	target->color = mix.color;// *(1 / (1 + DECAY * DT));



	/*
	float l = length(point->position - float2{ BBOXSIZE / 2, BBOXSIZE / 2 });
	if (l < 10) {
		float2 force{ 0, -50 };
		target->velocity = target->velocity + force * DT;
	}*/
}

