#pragma once


#define DENSITY 1
#define VISCOCITY 0.01
#define VORTICITY 0

#define DECAY 0.00001

#define BLOCK_SIZE 64
#define DT 0.5f

#define BBOXSIZE (6*BLOCK_SIZE)